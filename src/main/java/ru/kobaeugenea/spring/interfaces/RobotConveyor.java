package ru.kobaeugenea.spring.interfaces;

public interface RobotConveyor {

	Robot createRobot();

}
