package ru.kobaeugenea.spring.impls.sony;

import ru.kobaeugenea.spring.interfaces.Hand;

public class SonyHand implements Hand {

	public void catchSomething() {
		System.out.println("Catched from Sony!!");
	}

}
