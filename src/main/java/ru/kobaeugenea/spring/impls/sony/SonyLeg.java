package ru.kobaeugenea.spring.impls.sony;

import ru.kobaeugenea.spring.interfaces.Leg;

public class SonyLeg implements Leg {

	public void go() {
		System.out.println("Go to Sony!");
	}

}
