package ru.kobaeugenea.spring.impls.toshiba;

import org.springframework.stereotype.Component;

import ru.kobaeugenea.spring.interfaces.Hand;

@Component
public class ToshibaHand implements Hand {

	public void catchSomething() {
		System.out.println("Catched from Toshiba!");
	}

}
