package ru.kobaeugenea.spring.impls.conveyor;

import ru.kobaeugenea.spring.interfaces.RobotConveyor;
import ru.kobaeugenea.spring.interfaces.Robot;

public abstract class T1000Conveyor implements RobotConveyor {

	@Override
	public abstract Robot createRobot();

}
